const { appendFile } = require("fs");

const fs = require("fs").promises;

async function Problem2(filePath) {
  try {
    let data = await fs.readFile(filePath, "utf-8");
    console.log("Your File is ready to read");
    await fs.writeFile("upperCase.txt", data.toUpperCase());

    console.log("your file 1 data in uppercase:");
    await fs.writeFile("filenames.txt", "upperCase.txt");

    console.log("your filenames is created");
    data = await fs.readFile("upperCase.txt", "utf-8");

    let lowerConvert = data.toLowerCase().split(".");
    let lowerString = lowerConvert.map((lower) => lower.trim()).join("\n");
    await fs.writeFile("lowerCase.txt", lowerString);

    await fs.appendFile("./filenames.txt", "\nlowerCase.txt");

    data = await fs.readFile("lowerCase.txt", "utf-8");

    let sortContent = data.split("\n");
    sortContent = sortContent.sort().join("\n");
    await fs.writeFile("sortfile.txt", sortContent);

    await fs.appendFile("./filenames.txt", "\nsortfile.txt");

    console.log("complete 1");
    data = await fs.readFile("filenames.txt", "utf-8");

    let numberOfFiles = data.trim().split("\n");
    numberOfFiles.forEach(async (eachFile) => {
      await fs.unlink(eachFile);
      console.log(`${eachFile} deleted successfully`);
    });
  } catch (err) {
    console.error(err);
  }
}
module.exports = Problem2;
