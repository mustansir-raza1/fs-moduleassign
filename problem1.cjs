const fs = require("fs").promises;
async function createJSONFile(dirPath) {
  await fs.mkdir(dirPath, { recursive: true });
  console.log("Directory Created!");

  await fs.writeFile(`firstFile.txt`, "hello");
  console.log("Your file is created");

  await fs.unlink("firstFile.txt");
  console.log("your first file is deleted sucessfully");

  await fs.writeFile("secondFile.txt", "second file");
  console.log("your second file is created");

  await fs.unlink("secondFile.txt");
  console.log("your second file is deleted sucessfully");
}
module.exports = createJSONFile;
